const route = require("express").Router();
const knex = require("../config/database");
const auth = require("../services/auth.service");

route.post("/api/auth/login", async (req, res) => {
  console.log(req.body);
  if (!req.body.email || !req.body.password) {
    return res.status(400).send({
      success: false,
      message: "validate failed",
    });
  }

  let user;
  try {
    user = await knex("users").where("email", req.body.email).first();
  } catch (error) {
    return res.status(500).send({
      success: false,
      message: error.message,
      code: error?.code,
    });
  }

  if (!user) {
    console.log("no user");
    return res.status(401).send({
      success: false,
      message: "email or password incorrect",
    });
  }
  console.log(user);
  if (!auth.verifyPassword(req.body.password, user.password)) {
    return res.status(401).send({
      success: false,
      message: "email or password incorrect",
    });
  }

  try {
    const payload = {
      user_id: user.id,
      username: user.username,
      email: user.email,
      role: user.user_type,
    };
    const token = auth.generateToken(payload);
    return res.send({
      success: true,
      data: {
        token,
        ...payload,
      },
    });
  } catch (error) {
    return res.status(500).send({
      success: false,
      message: "Something went wrong",
      code: error?.code,
    });
  }
});

route.get("/api/auth/refresh", async (req, res) => {
  // api refresh jwt token
  const token = auth.getTokenHeader(req);
  console.log(token);
  if (!token) {
    return res.status(401).send({
      success: false,
      message: "Unauthorized",
    });
  }
  const newToken = auth.refreshTokenValidate(token);
  if (!newToken) {
    return res.status(401).send({
      success: false,
      message: "Unauthorized",
    });
  }
  return res.send({
    success: true,
    data: {
      token: newToken,
    },
  });
});

module.exports = route;
