const route = require("express").Router();
const auth = require("./auth");

route.use(auth);

module.exports = route;
