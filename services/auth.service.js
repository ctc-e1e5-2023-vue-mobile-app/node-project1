const jwt = require("jsonwebtoken");
const crypto = require("crypto");
const bcrypt = require("bcryptjs");
const secret = process.env.JWT_TOKEN_SECRET;

/**
 * Retrieves the token header from the request object.
 *
 * @param {Object} req - The request object.
 * @return {string} The token extracted from the authorization header.
 */
function getTokenHeader(req) {
  return req.headers["authorization"].replace("Bearer ", "");
}

/**
 * Generates a token using the provided payload.
 *
 * @param {object} payload - The payload to be used for generating the token.
 * @return {string} The generated token.
 */
function generateToken(payload) {
  payload.jti = payload.jti || crypto.randomUUID();
  return jwt.sign(payload, secret, {
    expiresIn: "5m",
    algorithm: "HS256",
    // notBefore: "30s",
    // jwtid: crypto.randomUUID(),
  });
}

/**
 * Verifies a token using the provided secret.
 *
 * @param {string} token - The token to be verified.
 * @return {object} - The verified token payload.
 */
function verifyToken(token) {
  return jwt.verify(token, secret);
}

/**
 * Validates and refreshes a token.
 *
 * @param {string} token - The token to be validated and refreshed.
 * @return {string|null} The new token if the validation and refresh are successful, otherwise null.
 */
function refreshTokenValidate(token) {
  try {
    const decode = verifyToken(token);

    delete decode.iat; // iat is issued at
    delete decode.exp; // exp is expiration
    delete decode.nbf; // nbf is not before
    // delete decode.jti; // jti is jwt id

    const newToken = generateToken(decode);
    return newToken;
  } catch (error) {
    return null;
  }
}

/**
 * Verifies if a given password matches a hash value.
 *
 * @param {string} password - The password to be verified.
 * @param {string} hash - The hash value to compare against.
 * @return {boolean} Returns true if the password matches the hash value, otherwise false.
 */
function verifyPassword(password, hash) {
  return bcrypt.compareSync(password, hash);
}

/**
 * Hashes a password using bcrypt.
 *
 * @param {string} password - The password to be hashed.
 * @return {string} The hashed password.
 */
function hashPassword(password) {
  return bcrypt.hashSync(password, 10);
}

module.exports = {
  getTokenHeader,
  generateToken,
  verifyToken,
  refreshTokenValidate,
  verifyPassword,
  hashPassword,
};
