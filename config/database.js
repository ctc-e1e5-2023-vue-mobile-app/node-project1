const knex = require("knex")({
  client: "mysql2",
  connection: {
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASS,
    database: process.env.MYSQL_DB,
    // supportBigNumber: true,
    timezone: "+07:00",
    dateStrings: true,
    charset: process.env.MYSQL_CHARSET,
  },
});

module.exports = knex;
