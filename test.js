const dotenv = require("dotenv");
dotenv.config();
const auth = require("./services/auth.service");
const testGenerator = auth.generateToken({ user_id: 5, username: "admin" });
console.log(testGenerator);
console.log("====================================");

// const token =
//   "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo1LCJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNjk5MDY1OTkzLCJuYmYiOjE2OTkwNjYwMjMsImV4cCI6MTY5OTA2NjI5M30.FUWy2pXlCJuh5iigP-IYYIxZoe_eRK7emr-_1nZxeV8";
const token = testGenerator;
const testVerify = auth.verifyToken(token);
console.log(testVerify);

console.log("====================================");
const testRefresh = auth.refreshTokenValidate(token);
console.log(testRefresh);
console.log(auth.verifyToken(testRefresh));

console.log("====================================");
console.log(
  auth.verifyPassword(
    "12345678",
    "$2y$10$3Rrp21DZXiQtqaRBAOX5qOct3TvstP60d.CVc42UR.FFTJO83mAKW"
  )
);
