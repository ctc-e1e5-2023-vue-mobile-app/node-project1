require("dotenv").config();
const cors = require("cors");
const bodyParser = require("body-parser");
const routes = require("./routes");
const express = require("express");
const app = express();
const port = process.env.PORT || 7100;

app.use(cors());
app.use(cors());
app.use(bodyParser.json());
app.use(express.static(__dirname + "/public"));

app.use(routes);

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
